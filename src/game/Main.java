package game;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;


import javax.swing.*;


public class Main {


    public static final int WINDOW_WIDTH = 700;
    public static final int WINDOW_HEIGHT = 360;
    public static final String WINDOW_TITLE = "Super Mario";
    public static Platform scene;
    public static JLabel labelScore;
    public static JLabel labelGameOver;
    public static JButton buttonNewGame;
    public static JButton buttonCloseGame;
    public static JFrame windowGame;


    public Main() {

        windowGame = new JFrame(WINDOW_TITLE);
        windowGame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        windowGame.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        windowGame.setLocationRelativeTo(null);
        windowGame.setResizable(true);
        windowGame.setAlwaysOnTop(true);

        labelScore = new JLabel();
        labelScore.setFont(new Font("Courier New", Font.BOLD, 23));
        labelScore.setForeground(Color.YELLOW);
        windowGame.add(labelScore);


        labelGameOver = new JLabel();
        labelGameOver.setFont(new Font("Courier New", Font.BOLD, 50));
        labelGameOver.setForeground(Color.RED);
        labelGameOver.setText("Game Over!");
        labelGameOver.setVisible(false);
        windowGame.add(labelGameOver);

        buttonNewGame = new JButton("Nuovo Gioco");
        buttonNewGame.setVisible(false);

        buttonCloseGame = new JButton("CHIUDE");
        buttonCloseGame.setVisible(false);

        scene = new Platform();
        scene.add(labelScore, BorderLayout.NORTH);
        scene.add(labelGameOver, BorderLayout.SOUTH);
        scene.add(buttonNewGame, BorderLayout.CENTER);
        scene.add(buttonCloseGame, BorderLayout.CENTER);
        windowGame.setContentPane(scene);

        windowGame.setVisible(true);

        Thread timer = new Thread(new Refresh());
        timer.start();
    }

    public static void main(String[] args) {
        new Main();
    }


}

package game;

public class Refresh implements Runnable {
    private volatile boolean running = true;
    private final int PAUSE = 3;


    public void run() {
        while (running) {
            Main.scene.repaint();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
                running = false;
            }
        }
    }

    public void replayGame() {
        Main.windowGame.dispose();
        running = false;
        new Main();
    }

    public void terminate() {
        running = false;
    }

} 
